REPO = gr4unch3r
IMAGE_NAME = vagrant-env
VERSION = latest

.PHONY: build run

build:
	docker build -t $(REPO)/$(IMAGE_NAME):$(VERSION) -f Dockerfile .

run:
	docker run --privileged \
	-v /dev/vboxdrv:/dev/vboxdrv \
	-v /dev/vboxnetctl:/dev/vboxnetctl \
	-e DISPLAY=$DISPLAY \
	-v /tmp/.X11-unix:/tmp/.X11-unix \
	-it $(REPO)/$(IMAGE_NAME):$(VERSION) \
	/bin/bash -c "vagrant up"

default: build
